package com.example.embededjasperfont.model;

import java.util.Date;

import lombok.Data;

@Data
public class Student {

	private String firstname;
	private String lastname;
	private Date dob;
}
