package com.example.embededjasperfont.controller;

import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.embededjasperfont.service.JasperGenerator;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class HomeController {
	
	@Autowired
	private JasperGenerator jasperGenerator;
	 
	@GetMapping("/jasper")
	public ResponseEntity<OutputStream> getEmbededJasper(HttpServletResponse response)
	{
		log.info("----calling ----");
		jasperGenerator.getJasperReport(response);
		
	  return new ResponseEntity<>(HttpStatus.OK);
	
		
	}


}
