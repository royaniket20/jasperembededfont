package com.example.embededjasperfont.service;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import com.example.embededjasperfont.model.Student;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.export.DocxExporterConfiguration;
import net.sf.jasperreports.export.SimpleDocxExporterConfiguration;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;

@Component
public class JasperGenerator {

	public void getJasperReport(HttpServletResponse response)
	{
		try {
		JRDocxExporter docxExporter = new JRDocxExporter();
		File file = ResourceUtils.getFile("classpath:SAMPLE.jrxml");
		JasperReport jasperReport =JasperCompileManager.compileReport(file.getAbsolutePath()); 
		ArrayList<Student> beanCollection = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			Student student = new Student();
			student.setDob(new Date());
			student.setFirstname("FIRST NAME"+i);
			student.setLastname("LAST NAME"+i);
			beanCollection.add(student);
		}
		HashMap<String, Object> parameters = new HashMap<String,Object>();
		parameters.put("dummy", "DUMMY HEADER");
		JasperPrint jrPrint = JasperFillManager.fillReport(jasperReport, parameters,new JRBeanCollectionDataSource(beanCollection));
		docxExporter.setExporterInput(new SimpleExporterInput(jrPrint));
		docxExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));
		response.setContentType("application/msword");
		response.addHeader("Content-Disposition", "attachment; filename=SAMPLE_"+new Random().nextInt()+".docx");
		docxExporter.exportReport(); 
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
}
